from .utils import (
    get_tld,
    get_tld_names,
    Result,
    update_tld_names,
)

__title__ = 'tld'
__version__ = '0.7.10'
__author__ = 'Artur Barseghyan'
__copyright__ = '2013-2018 Artur Barseghyan'
__license__ = 'GPL 2.0/LGPL 2.1'
__all__ = (
    'get_tld',
    'get_tld_names',
    'Result',
    'update_tld_names',
)
